﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ActionMailer.Net;
using ActionMailer.Net.Mvc;

namespace MyReleaseManager.Controllers
{
    public static class ExtensionMethods
    {
        public static string ToPublicUrl(this UrlHelper urlHelper, Uri relativeUri)
        {
            var httpContext = urlHelper.RequestContext.HttpContext;

            var uriBuilder = new UriBuilder
            {
                Host = httpContext.Request.Url.Host,
                Path = "/",
                Port = 80,
                Scheme = "http",
            };

            if (httpContext.Request.IsLocal)
            {
                uriBuilder.Port = httpContext.Request.Url.Port;
            }

            return new Uri(uriBuilder.Uri, relativeUri).AbsoluteUri;
        }
    }

    public class MailController : MailerBase
    {




        public EmailResult NewComment(string release, EamilInfo model)
        {
            try
            {
                To.Add("narora@fareportal.com");
                To.Add("ritesh.kumar@fareportal.com");
                Subject = "New comment: " + release + " release";
                From = "releasemanager@fareportal.com";
                
                var urlhelper = new UrlHelper(base.HttpContextBase.Request.RequestContext);
                // ViewBag.ReleasePageUrl = urlhelper.Action("Release", "Home", new { id = release.Id });
                // ViewBag.ReleasePageUrl = urlhelper.ToPublicUrl(new Uri(ViewBag.ReleasePageUrl, UriKind.Relative));
                // var aa = Email("NewComment");
                return Email("NewComment", model);
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }
    public class EamilInfo
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string FromEamil { get; set; }
        public string ToEamil { get; set; }
    }
}
