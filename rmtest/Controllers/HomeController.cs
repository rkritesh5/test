﻿using MyReleaseManager.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;

namespace rmtest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            MyReleaseManager.Controllers.EamilInfo obj=new EamilInfo();
             new MailController().NewComment("ABC", obj).Deliver();
                //if (email != null)
                //    new Thread(new ThreadStart(email.Deliver)).Start();
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}