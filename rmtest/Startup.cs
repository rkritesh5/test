﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(rmtest.Startup))]
namespace rmtest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
